/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.template.http.handlers;

import com.sun.net.httpserver.HttpExchange;
import com.uptime.services.http.handler.AbstractGenericHandler;
import static com.uptime.template.ServiceTemplate.LOGGER;
import static com.uptime.template.ServiceTemplate.running;
import java.io.IOException;
import java.util.logging.Logger;

/**
 *
 * @author twilcox
 */
public class MainHandler extends AbstractGenericHandler {
    //***********************************TO DO*************************************
    // The methods for handling GET, POST, PUT, and DELETE requests are currently not implemented.
    // To implement, uncomment the method needed and provide the coding.
    //*****************************************************************************
    
    
    /**
     * HTTP GET handler
     * @param he
     * @throws IOException 
     */
    //@Override
    //public void doGet(HttpExchange he) throws IOException {
    //}
    
    
    /**
     * HTTP POST handler
     * @param he
     * @throws IOException 
     */
    //@Override
    //public void doPost(HttpExchange he) throws IOException {
    //}
    
    
    /**
     * HTTP PUT handler
     * @param he
     * @throws IOException 
     */
    //@Override
    //public void doPut(HttpExchange he) throws IOException {
    //}
    
    
    /**
     * HTTP DELETE handler
     * @param he
     * @throws IOException 
     */
    //@Override
    //public void doDelete(HttpExchange he) throws IOException {
    //}
    
    /**
     * Returns the field 'running'
     * @return boolean
     */
    @Override
    public boolean getRunning() {
        return running;
    }

    /**
     * Returns the field 'LOGGER'
     * @return Logger Object
     */
    @Override
    public Logger getLogger() {
        return LOGGER;
    }
}
