/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.template.http.listeners;

import com.sun.net.httpserver.HttpServer;
import static com.uptime.template.ServiceTemplate.LOGGER;
import com.uptime.template.http.handlers.MainHandler;
import com.uptime.template.http.handlers.SystemHandler;
import java.net.InetSocketAddress;
import java.util.concurrent.Executors;
import java.util.logging.Level;

/**
 *
 * @author twilcox
 */
public class RequestListener {
    private int port = 0;
    HttpServer server;
    
    /**
     * Parameterized Constructor
     * @param port, int 
     */
    public RequestListener(int port) {
        this.port = port;
    }
    
    /**
     * Stop the Http server
     */
    public void stop() {
        LOGGER.log(Level.INFO, "Stopping request listener...");
        server.stop(10);
    }
    
    /**
     * Start the Http Server with the needed handlers
     * @throws Exception 
     */
    public void start() throws Exception {
    //***********************************TO DO*************************************
    // To add a new handler, create the class in the package "handler". The new class
    // needs to extend the abstract class AbstractGenericHandler. All abstract methods need to be
    // implemented. Once the class has been created, instantiate it in this method and add the
    // needed path to reach the handler.
    //
    // Note: The SystemHandler.java is the only handler that need to extend the abstract method
    // AbstractSystemHandler.
    //*****************************************************************************
    
        server = HttpServer.create(new InetSocketAddress(port), 2);
        server.createContext("/system", new SystemHandler());
        server.createContext("/main", new MainHandler());
        server.setExecutor(Executors.newCachedThreadPool());
        
        //start server
        LOGGER.log(Level.INFO, "Starting request listener...");
        server.start();
    }
}
